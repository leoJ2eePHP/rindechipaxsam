<?php

namespace app\models;

/**
 * Description of FlujoCajaCartola
 *
 * @author leand
 */
class FlujoCajaCartola {

    public $abono;
    public $cargo;
    public $descripcion;
    public $fecha;
    public $id;
    public $cuenta_corriente_id;
    public $tipo_cartola_id;
    public $ots = array();
    public $dtes = array();
    public $cartolaHija = array();
    public $cartolaMadre = array();
    public $compras = array();
    public $gastos = array();
    public $honorarios = array();
    public $boletaTerceros = array();
    public $remuneracions = array();
    public $previreds = array();
    public $impuestos = array();
    public $validacionesSaldos = array();
    public $saldos = array();
    //public $sincronizado = false;   // atributo que indicará si se encuentra coincidencia con los datos de RindeGastos
    public $sincronizacion_id_anterior; // ID de la sicronización anterior, para hacer la comparación y saber si hubo modificación

    const CATEGORIAS_COMBUSTIBLES_CHIPAX = [
        77859 => '01 Bencina',
        77469 => '02 Petróleo',
        106808 => '02.a Gas Licuado Vehicular',
        75820 => 'Cop. 01 Bencina',
        124960 => 'Cop. 02 Petróleo',
        142332 => 'Cop. 02.a Gas Licuado Vehicular',
        77470 => 'Cop. 03.1 Parafina',
        77858 => 'CG.- 01 Bencina',
        86680 => 'CG.- 02 Petróleo',
    ];

    public static function convert2Model($jsonArreglo, $fecha_desde = null, $fecha_hasta = null) {
        $data = array();
        $sw = false;
        $flujoCajaCartola = null;
        $folios = array();   // para verificar si existe algún folio repetido
        
        $anterior = Sincronizacion::find()->max("id");
        foreach ($jsonArreglo as $json) {
            foreach ($json["Compras"] as $c) {
                $fecha_emision = \app\components\Helper::formatToDBDate($c["fecha_emision"]);
                if ($fecha_desde !== null) {
                    if ($fecha_emision < $fecha_desde) {
                        $sw = false;
                        break;
                    }
                }
                if ($fecha_hasta !== null) {
                    if ($fecha_emision > $fecha_hasta) {
                        $sw = false;
                        break;
                    }
                }

                foreach ($c["Prorratas"] as $p) {
                    if ($p["linea_negocio_id"] == 5671 || array_key_exists($p["cuenta_id"], self::CATEGORIAS_COMBUSTIBLES_CHIPAX)) {
                        $sw = true;
                        break;
                    }
                }

                if ($sw)
                    break;
            }

            if (!$sw) {
                foreach ($json["Gastos"] as $g) {
                    $fecha_emision = \app\components\Helper::formatToDBDate($g["fecha"]);
                    if ($fecha_desde !== null) {
                        if ($fecha_emision < $fecha_desde) {
                            break;
                        }
                    }
                    if ($fecha_hasta !== null) {
                        if ($fecha_emision > $fecha_hasta) {
                            break;
                        }
                    }

                    foreach ($g["Prorratas"] as $p) {
                        if ($p["linea_negocio_id"] == 5671 || array_key_exists($p["cuenta_id"], self::CATEGORIAS_COMBUSTIBLES_CHIPAX)) {
                            $sw = true;
                            break;
                        }
                    }

                    if ($sw)
                        break;
                }
            }

            if (!$sw) {
                foreach ($json["Honorarios"] as $h) {
                    $fecha_emision = \app\components\Helper::formatToDBDate($h["fecha_emision"]);
                    if ($fecha_desde !== null) {
                        if ($fecha_emision < $fecha_desde) {
                            break;
                        }
                    }
                    if ($fecha_hasta !== null) {
                        if ($fecha_emision > $fecha_hasta) {
                            break;
                        }
                    }

                    foreach ($h["Prorratas"] as $p) {
                        if ($p["linea_negocio_id"] == 5671) {
                            $sw = true;
                            break;
                        }
                    }

                    if ($sw)
                        break;
                }
            }

            if (!$sw) {
                foreach ($json["Remuneracions"] as $r) {
                    $fecha_emision = \app\components\Helper::formatToDBDate($json["fecha"]);
                    if ($fecha_desde !== null) {
                        if ($fecha_emision < $fecha_desde) {
                            break;
                        }
                    }
                    if ($fecha_hasta !== null) {
                        if ($fecha_emision > $fecha_hasta) {
                            break;
                        }
                    }

                    foreach ($r["Prorratas"] as $p) {
                        if ($p["linea_negocio_id"] == 5671 || array_key_exists($p["cuenta_id"], self::CATEGORIAS_COMBUSTIBLES_CHIPAX)) {
                            $sw = true;
                            break;
                        }
                    }

                    if ($sw)
                        break;
                }
            }

//            if (!$sw) {
//                foreach ($json["Previreds"] as $previ) {
//                    $fecha_emision = \app\components\Helper::formatToDBDate($json["fecha"]);
//                    if ($fecha_desde !== null) {
//                        if ($fecha_emision < $fecha_desde) {
//                            break;
//                        }
//                    }
//                    if ($fecha_hasta !== null) {
//                        if ($fecha_emision > $fecha_hasta) {
//                            break;
//                        }
//                    }
//
//                    foreach ($previ["Prorratas"] as $p) {
//                        if ($p["linea_negocio_id"] == 5671 || array_key_exists($p["cuenta_id"], self::CATEGORIAS_COMBUSTIBLES_CHIPAX)) {
//                            $sw = true;
//                            break;
//                        }
//                    }
//
//                    if ($sw)
//                        break;
//                }
//            }

            if (!$sw)
                continue;

            $flujoCajaCartola = new FlujoCajaCartola();
            $flujoCajaCartola->abono = $json["abono"];
            $flujoCajaCartola->cargo = $json["cargo"];
            $flujoCajaCartola->descripcion = $json["descripcion"];
            $flujoCajaCartola->fecha = $json["fecha"];
            $flujoCajaCartola->id = $json["id"];
            $flujoCajaCartola->cuenta_corriente_id = $json["cuenta_corriente_id"];
            $flujoCajaCartola->tipo_cartola_id = $json["tipo_cartola_id"];
            $flujoCajaCartola->sincronizacion_id_anterior = $anterior;

            foreach ($json["Dtes"] as $dt) {
                $dtes = new DtesChipax();
                $dtes->folio = $dt["folio"];
                $dtes->id = $dt["id"];
                $dtes->monto_total = $dt["monto_total"];
                $dtes->razon_social = $dt["razon_social"];
                $dtes->rut = $dt["rut"];
                $dtes->fecha_emision = $dt["fecha_emision"];
                $dtes->tipo = $dt["tipo"];

                foreach ($dt["Ots"] as $ot) {
                    $ots = new OtsChipax();
                    $ots->monto_total = $ot["monto_total"];
                    $ots->borrada = $ot["borrada"];
                    $ots->cliente_normalizado_id = $ot["cliente_normalizado_id"];
                    $ots->fecha = $ot["fecha"];
                    $ots->fecha_pago = $ot["fecha_pago"];
                    $ots->folio = $ot["folio"];
                    $ots->id = $ot["id"];
                    $ots->iva = $ot["iva"];
                    $ots->moneda_id = $ot["moneda_id"];
                    $ots->monto_neto = $ot["monto_neto"];
                    $ots->otro_ingreso = $ot["otro_ingreso"];
                    $ots->created = $ot["created"];
                    $ots->deleted = $ot["deleted"];
                    $ots->deleted_date = $ot["deleted_date"];

                    foreach ($ot["Movimientos"] as $mov) {
                        $movimiento = new MovimientosChipax();
                        $movimiento->monto_neto = $mov["monto_neto"];
                        $movimiento->cantidad = $mov["cantidad"];
                        $movimiento->descuento = $mov["cantidad"];
                        $movimiento->detalle = $mov["cantidad"];
                        $movimiento->id = $mov["cantidad"];
                        $movimiento->linea_negocio_id = $mov["cantidad"];
                        $movimiento->moneda_id = $mov["cantidad"];
                        $movimiento->ot_id = $mov["cantidad"];
                        $movimiento->precio_unitario = $mov["cantidad"];
                        $movimiento->producto_id = $mov["cantidad"];
                        $movimiento->valor_moneda = $mov["cantidad"];
                        $movimiento->cliente_normalizado_id = $mov["cantidad"];

                        $prod = $mov["Producto"];
                        $producto = new ProductoChipax();
                        $producto->cuenta_id = $prod["cuenta_id"];
                        $producto->id = $prod["id"];
                        $producto->nombre = $prod["nombre"];
                        $producto->otro_ingreso = $prod["otro_ingreso"];
                        $producto->tipo = $prod["tipo"];

                        $movimiento->producto = $producto;
                        $ots->movimientos[] = $movimiento;
                    }

                    $dteOt = new DteOtChipax();
                    $dteOt->id = $ot["DteOt"]["id"];
                    $dteOt->foreign_key = $ot["DteOt"]["foreign_key"];
                    $dteOt->modelo = $ot["DteOt"]["modelo"];
                    $dteOt->ot_id = $ot["DteOt"]["ot_id"];
                    $ots->dteOt = $dteOt;

                    $dtes->ots[] = $ots;

                    $cartolaDocumento = new CartolaDocumentoChipax();
                    $cartolaDocumento->cartola_id = $dt["CartolaDocumento"]["cartola_id"];
                    $cartolaDocumento->dias_pago = $dt["CartolaDocumento"]["dias_pago"];
                    $cartolaDocumento->foreign_key = $dt["CartolaDocumento"]["foreign_key"];
                    $cartolaDocumento->modelo = $dt["CartolaDocumento"]["modelo"];
                    $cartolaDocumento->monto_convertido = $dt["CartolaDocumento"]["monto_convertido"];
                    $cartolaDocumento->monto = $dt["CartolaDocumento"]["monto"];
                    $cartolaDocumento->tipo_cambio = $dt["CartolaDocumento"]["tipo_cambio"];
                    $cartolaDocumento->usuario_id = $dt["CartolaDocumento"]["usuario_id"];
                    $dtes->cartolaDocumento = $cartolaDocumento;
                }

                $flujoCajaCartola->dtes[] = $dtes;
            }

            foreach ($json["CartolaHija"] as $cartoHija) {
                $cartolaHija = new CartolaHijaChipax();
                $cartolaHija->abono = $cartoHija["abono"];
                $cartolaHija->cargo = $cartoHija["cargo"];
                $cartolaHija->descripcion = $cartoHija["descripcion"];
                $cartolaHija->fecha = $cartoHija["fecha"];
                $cartolaHija->id = $cartoHija["id"];
                $cartolaHija->cuenta_corriente_id = $cartoHija["cuenta_corriente_id"];
                $cartolaHija->tipo_cartola_id = $cartoHija["tipo_cartola_id"];

                $cartolaDoc = new CartolaDocumentoChipax();
                $cartolaDoc->cartola_id = $cartoHija["CartolaDocumento"]["cartola_id"];
                $cartolaDoc->dias_pago = $cartoHija["CartolaDocumento"]["dias_pago"];
                $cartolaDoc->foreign_key = $cartoHija["CartolaDocumento"]["foreign_key"];
                $cartolaDoc->modelo = $cartoHija["CartolaDocumento"]["modelo"];
                $cartolaDoc->monto_convertido = $cartoHija["CartolaDocumento"]["monto_convertido"];
                $cartolaDoc->monto = $cartoHija["CartolaDocumento"]["monto"];
                $cartolaDoc->tipo_cambio = $cartoHija["CartolaDocumento"]["tipo_cambio"];
                $cartolaDoc->usuario_id = $cartoHija["CartolaDocumento"]["usuario_id"];
                $cartolaHija->cartolaDocumento = $cartolaDoc;

                $flujoCajaCartola->cartolaHija[] = $cartolaHija;
            }

            foreach ($json["Compras"] as $c) {
                try {   // este bloque evitará que haya un folio duplicado mostrándose
                    if (array_search($c["folio"], $folios) !== false) {
                        continue;
                    }
                } catch (\yii\base\ErrorException $ex) {
                    echo "<pre>";
                    print_r($ex);
                    break;
                }

                if ($fecha_desde !== null) {
                    if ($c["fecha_emision"] < $fecha_desde) {
                        $sw = false;
                        break;
                    }
                }
                if ($fecha_hasta !== null) {
                    if ($c["fecha_emision"] > $fecha_hasta) {
                        $sw = false;
                        break;
                    }
                }

                $compras = new ComprasChipax();
                $compras->fecha_emision = $c["fecha_emision"];
                $compras->folio = $c["folio"];
                $compras->id = $c["id"];
                $compras->moneda_id = $c["moneda_id"];
                $compras->monto_total = $c["monto_total"];
                $compras->razon_social = $c["razon_social"];
                $compras->rut_emisor = $c["rut_emisor"];
                $compras->tipo = $c["tipo"];

                foreach ($c["Prorratas"] as $pro) {
                    if ($pro["linea_negocio_id"] == 5671 || array_key_exists($p["cuenta_id"], self::CATEGORIAS_COMBUSTIBLES_CHIPAX)) {
                        $prorrata = new ProrrataChipax();
                        $prorrata->cuenta_id = $pro["cuenta_id"];
                        $prorrata->filtro_id = $pro["filtro_id"];
                        $prorrata->foreign_key = $pro["foreign_key"];
                        $prorrata->id = $pro["id"];
                        $prorrata->linea_negocio_id = $pro["linea_negocio_id"];
                        $prorrata->modelo = $pro["modelo"];
                        $prorrata->monto = $pro["monto"];
                        $prorrata->periodo = $pro["periodo"];
                        $compras->prorratas[] = $prorrata;
                    }
                }

                $cart = $c["CartolaDocumento"];
                $cartolaDocumento = new CartolaDocumentoChipax();
                $cartolaDocumento->cartola_id = $cart["cartola_id"];
                $cartolaDocumento->dias_pago = $cart["dias_pago"];
                $cartolaDocumento->foreign_key = $cart["foreign_key"];
                $cartolaDocumento->modelo = $cart["modelo"];
                $cartolaDocumento->monto_convertido = $cart["monto_convertido"];
                $cartolaDocumento->monto = $cart["monto"];
                $cartolaDocumento->tipo_cambio = $cart["tipo_cambio"];
                $cartolaDocumento->usuario_id = $cart["usuario_id"];

                $compras->cartolaDocumento = $cartolaDocumento;
                $flujoCajaCartola->compras[] = $compras;
                $folios[] = $c["folio"];
            }

            foreach ($json["Gastos"] as $g) {
                if ($fecha_desde !== null) {
                    if ($g["fecha"] < $fecha_desde) {
                        $sw = false;
                        break;
                    }
                }
                if ($fecha_hasta !== null) {
                    if ($g["fecha"] > $fecha_hasta) {
                        $sw = false;
                        break;
                    }
                }
                $gasto = new GastosChipax();
                $gasto->descripcion = $g["descripcion"];
                $gasto->fecha = $g["fecha"];
                $gasto->id = $g["id"];
                $gasto->moneda_id = $g["moneda_id"];
                $gasto->monto = $g["monto"];
                $gasto->num_documento = $g["num_documento"];
                $gasto->proveedor = $g["proveedor"];
                $gasto->responsable = $g["responsable"];
                $gasto->tipo_cambio = $g["tipo_cambio"];
                $gasto->usuario_id = $g["usuario_id"];

                foreach ($g["Prorratas"] as $pro) {
                    if ($pro["linea_negocio_id"] == 5671 || array_key_exists($p["cuenta_id"], self::CATEGORIAS_COMBUSTIBLES_CHIPAX)) {
                        $prorrata = new ProrrataChipax();
                        $prorrata->cuenta_id = $pro["cuenta_id"];
                        $prorrata->filtro_id = $pro["filtro_id"];
                        $prorrata->foreign_key = $pro["foreign_key"];
                        $prorrata->id = $pro["id"];
                        $prorrata->linea_negocio_id = $pro["linea_negocio_id"];
                        $prorrata->modelo = $pro["modelo"];
                        $prorrata->monto = $pro["monto"];
                        $prorrata->periodo = $pro["periodo"];

                        $gasto->prorratas[] = $prorrata;
                    }
                }

                $cartolaDoc = new CartolaDocumentoChipax();
                $cartolaDoc->cartola_id = $g["CartolaDocumento"]["cartola_id"];
                $cartolaDoc->dias_pago = $g["CartolaDocumento"]["dias_pago"];
                $cartolaDoc->foreign_key = $g["CartolaDocumento"]["foreign_key"];
                $cartolaDoc->modelo = $g["CartolaDocumento"]["modelo"];
                $cartolaDoc->monto_convertido = $g["CartolaDocumento"]["monto_convertido"];
                $cartolaDoc->monto = $g["CartolaDocumento"]["monto"];
                $cartolaDoc->tipo_cambio = $g["CartolaDocumento"]["tipo_cambio"];
                $cartolaDoc->usuario_id = $g["CartolaDocumento"]["usuario_id"];
                $gasto->cartolaDocumento = $cartolaDoc;

                $flujoCajaCartola->gastos[] = $gasto;
            }

            foreach ($json["Honorarios"] as $h) {
                if ($fecha_desde !== null) {
                    if ($h["fecha_emision"] < $fecha_desde) {
                        $sw = false;
                        break;
                    }
                }
                if ($fecha_hasta !== null) {
                    if ($h["fecha_emision"] > $fecha_hasta) {
                        $sw = false;
                        break;
                    }
                }

                $honorario = new HonorariosChipax();
                $honorario->fecha_emision = $h["fecha_emision"];
                $honorario->id = $h["id"];
                $honorario->moneda_id = $h["moneda_id"];
                $honorario->monto_liquido = $h["monto_liquido"];
                $honorario->numero_boleta = $h["numero_boleta"];
                $honorario->nombre_emisor = $h["nombre_emisor"];
                $honorario->rut_emisor = $h["rut_emisor"];
                $honorario->usuario_id = $h["usuario_id"];

                foreach ($h["Prorratas"] as $pro) {
                    if ($pro["linea_negocio_id"] == 5671 || array_key_exists($pro["cuenta_id"], self::CATEGORIAS_COMBUSTIBLES_CHIPAX)) {
                        $prorrata = new ProrrataChipax();
                        $prorrata->cuenta_id = $pro["cuenta_id"];
                        $prorrata->filtro_id = $pro["filtro_id"];
                        $prorrata->foreign_key = $pro["foreign_key"];
                        $prorrata->id = $pro["id"];
                        $prorrata->linea_negocio_id = $pro["linea_negocio_id"];
                        $prorrata->modelo = $pro["modelo"];
                        $prorrata->monto = $pro["monto"];
                        $prorrata->periodo = $pro["periodo"];

                        $honorario->prorratas[] = $prorrata;
                    }
                }

                $cartolaDoc = new CartolaDocumentoChipax();
                $cartolaDoc->cartola_id = $h["CartolaDocumento"]["cartola_id"];
                $cartolaDoc->dias_pago = $h["CartolaDocumento"]["dias_pago"];
                $cartolaDoc->foreign_key = $h["CartolaDocumento"]["foreign_key"];
                $cartolaDoc->modelo = $h["CartolaDocumento"]["modelo"];
                $cartolaDoc->monto_convertido = $h["CartolaDocumento"]["monto_convertido"];
                $cartolaDoc->monto = $h["CartolaDocumento"]["monto"];
                $cartolaDoc->tipo_cambio = $h["CartolaDocumento"]["tipo_cambio"];
                $cartolaDoc->usuario_id = $h["CartolaDocumento"]["usuario_id"];
                $honorario->cartolaDocumento = $cartolaDoc;

                $flujoCajaCartola->honorarios[] = $honorario;
            }

            foreach ($json["Remuneracions"] as $r) {
                if ($fecha_desde !== null) {
                    if ($json["fecha"] < $fecha_desde) {
                        $sw = false;
                        break;
                    }
                }
                if ($fecha_hasta !== null) {
                    if ($json["fecha"] > $fecha_hasta) {
                        $sw = false;
                        break;
                    }
                }

                $remuneracion = new RemuneracionsChipax();
                $remuneracion->id = $r["id"];
                $remuneracion->empresa_id = $r["empresa_id"];
                $remuneracion->usuario_id = $r["usuario_id"];
                $remuneracion->periodo = $r["periodo"];
                $remuneracion->empleado_id = $r["empleado_id"];
                $remuneracion->monto_liquido = $r["monto_liquido"];
                $remuneracion->moneda_id = $r["moneda_id"];
                $remuneracion->liquidacion = $r["liquidacion"];
                $remuneracion->created = $r["created"];
                $remuneracion->modified = $r["modified"];
                $remuneracion->comentario_count = $r["comentario_count"];

                foreach ($r["Prorratas"] as $pro) {
                    if ($pro["linea_negocio_id"] == 5671 || array_key_exists($pro["cuenta_id"], self::CATEGORIAS_COMBUSTIBLES_CHIPAX)) {
                        $prorrata = new ProrrataChipax();
                        $prorrata->cuenta_id = $pro["cuenta_id"];
                        $prorrata->filtro_id = $pro["filtro_id"];
                        $prorrata->foreign_key = $pro["foreign_key"];
                        $prorrata->id = $pro["id"];
                        $prorrata->linea_negocio_id = $pro["linea_negocio_id"];
                        $prorrata->modelo = $pro["modelo"];
                        $prorrata->monto = $pro["monto"];
                        $prorrata->periodo = $pro["periodo"];

                        $remuneracion->prorratas[] = $prorrata;
                    }
                }

                $cartolaDoc = new CartolaDocumentoChipax();
                $cartolaDoc->cartola_id = $r["CartolaDocumento"]["cartola_id"];
                $cartolaDoc->dias_pago = $r["CartolaDocumento"]["dias_pago"];
                $cartolaDoc->foreign_key = $r["CartolaDocumento"]["foreign_key"];
                $cartolaDoc->modelo = $r["CartolaDocumento"]["modelo"];
                $cartolaDoc->monto_convertido = $r["CartolaDocumento"]["monto_convertido"];
                $cartolaDoc->monto = $r["CartolaDocumento"]["monto"];
                $cartolaDoc->tipo_cambio = $r["CartolaDocumento"]["tipo_cambio"];
                $cartolaDoc->usuario_id = $r["CartolaDocumento"]["usuario_id"];
                $remuneracion->cartolaDocumento = $cartolaDoc;

                $empleado = new EmpleadoChipax();
                $empleado->id = $r["Empleado"]["id"];
                $empleado->nombre = $r["Empleado"]["nombre"];
                $empleado->apellido = $r["Empleado"]["apellido"];
                $empleado->rut = $r["Empleado"]["rut"];
                $empleado->email = $r["Empleado"]["email"];
                $remuneracion->empleado = $empleado;

                $flujoCajaCartola->remuneracions[] = $remuneracion;
            }

//            foreach ($json["Previreds"] as $previ) {
//                $previred = new Previreds();
//                $previred->id = $previ["id"];
//                $previred->empresa_id = $previ["empresa_id"];
//                $previred->usuario_id = $previ["usuario_id"];
//                $previred->periodo = $previ["periodo"];
//                $previred->monto = $previ["monto"];
//                $previred->moneda_id = $previ["moneda_id"];
//                $previred->planilla_imposiciones = $previ["planilla_imposiciones"];
//                $previred->created = $previ["created"];
//                $previred->modified = $previ["modified"];
//                $previred->comentario_count = $previ["comentario_count"];
//
//                $cartolaDoc = new CartolaDocumentoChipax();
//                $cartolaDoc->cartola_id = $previ["CartolaDocumento"]["cartola_id"];
//                $cartolaDoc->dias_pago = $previ["CartolaDocumento"]["dias_pago"];
//                $cartolaDoc->foreign_key = $previ["CartolaDocumento"]["foreign_key"];
//                $cartolaDoc->modelo = $previ["CartolaDocumento"]["modelo"];
//                $cartolaDoc->monto_convertido = $previ["CartolaDocumento"]["monto_convertido"];
//                $cartolaDoc->monto = $previ["CartolaDocumento"]["monto"];
//                $cartolaDoc->tipo_cambio = $previ["CartolaDocumento"]["tipo_cambio"];
//                $cartolaDoc->usuario_id = $previ["CartolaDocumento"]["usuario_id"];
//                $previred->cartolaDocumento = $cartolaDoc;
//
//                foreach ($previ["Prorratas"] as $pro) {
//                    if ($pro["linea_negocio_id"] == 5671 || array_key_exists($pro["cuenta_id"], self::CATEGORIAS_COMBUSTIBLES_CHIPAX)) {
//                        $prorrata = new ProrrataChipax();
//                        $prorrata->cuenta_id = $pro["cuenta_id"];
//                        $prorrata->filtro_id = $pro["filtro_id"];
//                        $prorrata->foreign_key = $pro["foreign_key"];
//                        $prorrata->id = $pro["id"];
//                        $prorrata->linea_negocio_id = $pro["linea_negocio_id"];
//                        $prorrata->modelo = $pro["modelo"];
//                        $prorrata->monto = $pro["monto"];
//                        $prorrata->periodo = $pro["periodo"];
//                        $previred->prorratas[] = $prorrata;
//                    }
//                }
//
//                $flujoCajaCartola->previreds[] = $previred;
//            }

            foreach ($json["Saldos"] as $s) {
                $saldo = new SaldosChipax();
                $saldo->id = $s["id"];
                $saldo->debe = $s["debe"];
                $saldo->haber = $s["haber"];
                $saldo->saldo_deudor = $s["saldo_deudor"];
                $saldo->saldo_acreedor = $s["saldo_acreedor"];
                $saldo->last_record = $s["last_record"];
                $saldo->modelo = $s["modelo"];
                $saldo->foreign_key = $s["foreign_key"];

                $flujoCajaCartola->saldos[] = $saldo;
            }

            $sw = false;    // para que vuelva a buscar solo si la linea de negocio es departamento de maquinaria
            //array_push($data, $flujoCajaCartola);
            $data[] = $flujoCajaCartola;
        }

        return $data;
    }

}
