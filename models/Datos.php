<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "datos".
 *
 * @property int $id
 * @property string $folio
 * @property int $monto
 * @property string $tipo_gasto
 * @property string|null $detalles
 * @property int $sincronizacion_id
 *
 * @property Sincronizacion $sincronizacion
 */
class Datos extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'datos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['folio', 'monto', 'sincronizacion_id'], 'required'],
            [['monto', 'sincronizacion_id'], 'integer'],
            [['folio'], 'string', 'max' => 45],
            [['tipo_gasto'], 'string', 'max' => 15],
            [['detalles'], 'string', 'max' => 100],
            [['sincronizacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sincronizacion::className(), 'targetAttribute' => ['sincronizacion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'folio' => 'Folio',
            'monto' => 'Monto',
            'tipo_gasto' => 'Tipo Gasto',
            'detalles' => 'Detalles',
            'sincronizacion_id' => 'Sincronizacion ID',
        ];
    }

    /**
     * Gets query for [[Sincronizacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSincronizacion() {
        return $this->hasOne(Sincronizacion::className(), ['id' => 'sincronizacion_id']);
    }

}
