<?php

namespace app\models;

/**
 * Description of OtsChipax
 *
 * @author leand
 */
class OtsChipax {
    
    public $monto_total;
    public $borrada;
    public $cliente_normalizado_id;
    public $fecha;
    public $fecha_pago;
    public $folio;
    public $id;
    public $iva;
    public $moneda_id;
    public $monto_neto;
    public $otro_ingreso;
    public $created;
    public $deleted;
    public $deleted_date;
    public $movimientos = array();
    public $dteOt;     //objeto
    
}
