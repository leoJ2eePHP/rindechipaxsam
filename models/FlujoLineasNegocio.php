<?php

namespace app\models;

/**
 * Description of DtesChipax
 *
 * @author leand
 */
class FlujoLineasNegocio {

    public $lineaNegocio;
    
    public static function convert2Model($jsonArreglo) {
        $resultado = array();
        foreach ($jsonArreglo as $linea) {
            $lineaNegocio = new LineaNegocio();
            
            $lineaNegocio->id = $linea["id"];
            $lineaNegocio->nombre = $linea["nombre"];
            $lineaNegocio->default = $linea["default"];
            $lineaNegocio->cerrada = $linea["cerrada"];
            $lineaNegocio->deleted = $linea["deleted"];
            
            $resultado[] = $lineaNegocio;
        }
        
        return $resultado;
    }

}
