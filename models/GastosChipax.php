<?php

namespace app\models;

/**
 * Description of GastoChipax
 *
 * @author leand
 */
class GastosChipax {
    
    public $descripcion;
    public $fecha;
    public $id;
    public $moneda_id;
    public $monto;
    public $num_documento;
    public $proveedor;
    public $responsable;
    public $tipo_cambio;
    public $usuario_id;
    public $prorratas = array();
    public $cartolaDocumento;   // objeto
    public $sincronizado = false;   // atributo que indicará si se encuentra coincidencia con los datos de RindeGastos
    
}
