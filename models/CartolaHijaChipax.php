<?php

namespace app\models;

/**
 * Description of CartolaHijaChipax
 *
 * @author leand
 */
class CartolaHijaChipax {
    
    public $abono;
    public $cargo;
    public $descripcion;
    public $fecha;
    public $id;
    public $cuenta_corriente_id;
    public $tipo_cartola_id;
    public $cartolaDocumento;  // objeto
    
}
