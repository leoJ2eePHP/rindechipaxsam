<?php

namespace app\models;

/**
 * Description of Saldos
 *
 * @author leand
 */
class SaldosChipax {
    
    public $id;
    public $debe;
    public $haber;
    public $saldo_deudor;
    public $saldo_acreedor;
    public $last_record;
    public $modelo;
    public $foreign_key;
    
}
