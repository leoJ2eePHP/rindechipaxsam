<?php

namespace app\models;

/**
 * Description of Empleado
 *
 * @author leand
 */
class EmpleadoChipax {
    
    public $id;
    public $nombre;
    public $apellido;
    public $rut;
    public $email;
    
}
