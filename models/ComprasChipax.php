<?php

namespace app\models;

/**
 * Description of ComprasChipax
 *
 * @author leand
 */
class ComprasChipax {

    public $fecha_emision;
    public $folio;
    public $id;
    public $moneda_id;
    public $monto_total;
    public $razon_social;
    public $rut_emisor;
    public $tipo;
    public $prorratas = array();
    public $cartolaDocumento;  //objeto
    public $sincronizado = false;   // atributo que indicará si se encuentra coincidencia con los datos de RindeGastos

}
