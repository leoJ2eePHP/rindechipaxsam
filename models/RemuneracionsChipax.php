<?php
namespace app\models;

/**
 * Description of Remuneracions
 *
 * @author leand
 */
class RemuneracionsChipax {
    
    public $id;
    public $empresa_id;
    public $usuario_id;
    public $periodo;
    public $empleado_id;
    public $monto_liquido;
    public $moneda_id;
    public $liquidacion;
    public $created;
    public $modified;
    public $comentario_count;
    public $cartolaDocumento;   // objeto
    public $prorratas = array();
    public $empleado;   // objeto
    public $sincronizado = false;   // atributo que indicará si se encuentra coincidencia con los datos de RindeGastos
    
}
