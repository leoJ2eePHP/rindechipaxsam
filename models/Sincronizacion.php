<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sincronizacion".
 *
 * @property int $id
 * @property string $fecha
 *
 * @property Datos[] $datos
 */
class Sincronizacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sincronizacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'required'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Datos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDatos()
    {
        return $this->hasMany(Datos::className(), ['sincronizacion_id' => 'id']);
    }
}
