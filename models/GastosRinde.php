<?php

namespace app\models;

/**
 * Description of GastosRinde
 *
 * @author leand
 */
class GastosRinde {
    
    public $id;
    public $status;
    public $supplier;
    public $issue_date;
    public $original_amount;
    public $original_currency;
    public $exchange_rate;
    public $net;
    public $tax;
    public $tax_name;
    public $other_taxes;
    public $retention_name;
    public $retention;
    public $total;
    public $currency;
    public $reimbursable;
    public $category;
    public $category_code;
    public $category_group;
    public $category_group_code;
    public $note;
    public $integration_date;
    public $integration_external_code;
    public $folio;
    
    public static function convert2Model($jsonArreglo) {
        /*
        foreach ($jsonArreglo as $json) {
            print_r($json->ExtraFields[2]->Value);
            die;
        }
        */
    }
    
}
