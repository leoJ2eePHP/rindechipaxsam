<?php

namespace app\models;

/**
 * Description of Previreds
 *
 * @author leand
 */
class Previreds {
    
    public $id;
    public $empresa_id;
    public $usuario_id;
    public $periodo;
    public $monto;
    public $moneda_id;
    public $planilla_imposiciones;
    public $created;
    public $modified;
    public $comentario_count;
    public $cartolaDocumento;   // objeto
    public $prorratas = array();
    public $sincronizado = false;   // atributo que indicará si se encuentra coincidencia con los datos de RindeGastos
    
}
