<?php

namespace app\models;

/**
 * Description of DtesChipax
 *
 * @author leand
 */
class DtesChipax {
    
    public $folio;
    public $id;
    public $monto_total;
    public $razon_social;
    public $rut;
    public $fecha_emision;
    public $tipo;
    public $ots = array();
    public $cartolaDocumento;  //objeto
    
}
