<?php

namespace app\models;

/**
 * Description of CartolaDocumentoChipax
 *
 * @author leand
 */
class CartolaDocumentoChipax {
    
    public $cartola_id;
    public $dias_pago;
    public $foreign_key;
    public $modelo;
    public $monto_convertido;
    public $monto;
    public $tipo_cambio;
    public $usuario_id;
    
}
