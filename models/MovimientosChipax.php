<?php

namespace app\models;

/**
 * Description of MovimientoChipax
 *
 * @author leand
 */
class MovimientosChipax {
    
    public $monto_neto;
    public $cantidad;
    public $descuento;
    public $detalle;
    public $id;
    public $linea_negocio_id;
    public $moneda_id;
    public $ot_id;
    public $precio_unitario;
    public $producto_id;
    public $valor_moneda;
    public $cliente_normalizado_id;
    public $producto;  // objeto
    
}
