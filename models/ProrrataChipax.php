<?php

namespace app\models;

/**
 * Description of ProrrataChipax
 *
 * @author leand
 */
class ProrrataChipax {
    
    public $cuenta_id;
    public $filtro_id;
    public $foreign_key;
    public $id;
    public $linea_negocio_id;
    public $modelo;
    public $monto;
    public $periodo;
    
}
