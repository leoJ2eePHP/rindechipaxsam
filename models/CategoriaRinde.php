<?php

namespace app\models;

use Yii;

/**
 * Description of GastosRinde
 *
 * @author leand
 */
class CategoriaRinde {
    
    public $Name;
    public $GroupName;
    public $GroupCode;
    public $AccountCode;
    public $Instructions;
    
    private $categorias = [];
    
    public static function getCategoriasByPolicyIdFromApi($idPolitica) {
        $api = new RindeGastos(Yii::$app->params["rindeGastosToken"]);
        $params = ["IdPolicy" => $idPolitica];
        $json = $api->getExpensePolicyCategories($params);
        $data = json_decode($json);
        foreach ($data->Categories as $cat) {
            $categorias[] = $cat;
        }
        
        return $categorias;
    }
    
    public static function getAllCategoriesFromApi() {
        $api = new RindeGastos(Yii::$app->params["rindeGastosToken"]);
        $params = ["Status" => 1, "ResultsPerPage" => 100];
        $json = $api->getExpensePolicies($params);
        $politicas = json_decode($json);
        
        foreach ($politicas->Policies as $p) {
            $categorias[] = self::getCategoriasFromApi($p->Id);
        }
        
        return $categorias;
    }
    
}
