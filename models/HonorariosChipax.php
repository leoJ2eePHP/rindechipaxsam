<?php

namespace app\models;

/**
 * Description of Honorarios
 *
 * @author leand
 */
class HonorariosChipax {
    
    public $fecha_emision;
    public $id;
    public $moneda_id;
    public $monto_liquido;
    public $numero_boleta;
    public $nombre_emisor;
    public $rut_emisor;
    public $usuario_id;
    public $prorratas = array();
    public $cartolaDocumento;   // Objeto
    public $sincronizado = false;   // atributo que indicará si se encuentra coincidencia con los datos de RindeGastos
    
}
