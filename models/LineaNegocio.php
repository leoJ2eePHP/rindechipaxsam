<?php

namespace app\models;

use Yii;

/**
 * Description of DtesChipax
 *
 * @author leand
 */
class LineaNegocio {

    public $id;
    public $nombre;
    public $default;
    public $cerrada;
    public $deleted;

    public static function getLineaNegocioById($linea_negocio_id) {
        $lineasNegocio = Yii::$app->session->get("LineasNegocio");
        foreach ($lineasNegocio as $negocio) {
            if ($negocio->id == $linea_negocio_id) {
                return $negocio;
            }
        }

        return null;
    }

}
