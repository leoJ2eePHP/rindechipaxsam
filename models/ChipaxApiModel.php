<?php

namespace app\models;

use app\models\FlujoCajaCartola;
use yii\httpclient\Client;

/**
 * Description of ChipaxApi
 *
 * @author leand
 */
class ChipaxApiModel {

    public $token;

    public function getFlujoCajaCartolas($start_date = null, $end_date = null) {
        $client = new Client(["baseUrl" => "https://api.chipax.com/flujo-caja/cartolas"]);

        $fecha_desde = null !== $start_date ? $start_date : date("Y-m-d", strtotime(date("Y-m-d") . " - 1 month"));
        $fecha_hasta = null !== $end_date ? $end_date : date("Y-m-d");
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                //->setData(["startDate" => '2021-06-01T00:00:00.000Z'])
                ->setData(["startDate" => $fecha_desde . 'T00:00:00.000Z', "endDate" => $fecha_hasta . 'T23:59:59.000Z'])
                //->setData(["startDate" => date('Y-m-d\TH:i:s')])
                ->send();

        if ($request->getData()["pages"] > 1) {
            for ($i = 1; $i <= $request->getData()["pages"]; $i++) {
                $request = $client->createRequest()
                                ->setHeaders(['content-type' => 'application/json'])
                                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                                ->setData(["startDate" => $fecha_desde . 'T00:00:00.000Z', "endDate" => $fecha_hasta . 'T23:59:59.000Z',
                                    "page" => $i])->send();
                $flujo[] = FlujoCajaCartola::convert2Model($request->getData()["docs"], $fecha_desde, $fecha_hasta);
            }
        } else {
            $flujo[] = FlujoCajaCartola::convert2Model($request->getData()["docs"], $fecha_desde, $fecha_hasta);
        }

        return $flujo;
    }

    public function getLineasDeNegocio() {
        $client = new Client(["baseUrl" => "https://api.chipax.com/lineas-negocio"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();

        $model = FlujoLineasNegocio::convert2Model($request->getData());
        return $model;
    }

    public function getLineasDeNegocioById($linea_negocio_id) {
        $client = new Client(["baseUrl" => "https://api.chipax.com/lineas-negocio"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();

        $model = FlujoLineasNegocio::convert2Model($request->getData());
        foreach ($model as $negocio) {
            if ($negocio->id == $linea_negocio_id) {
                return $negocio;
            }
        }
        
        return null;
    }
    
    public function getCategorias() {
        $client = new Client(["baseUrl" => "https://api.chipax.com/flujo-caja/categorias"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();
        
        return $request->getData();
    }

    private function generateToken() {
        $client = new Client();
        $this->token = $client->createRequest()
                ->setMethod("POST")
                ->setUrl("https://api.chipax.com/login")
                ->setData(["app_id" => \Yii::$app->params["app_id"], "secret_key" => \Yii::$app->params["secret_key"]])
                ->send();

        return $this->token->getData();
    }

    private function getToken() {
        if (!isset($this->token) || empty($this->token)) {
            $this->token = $this->generateToken();
        }

        return $this->token;
    }

}
