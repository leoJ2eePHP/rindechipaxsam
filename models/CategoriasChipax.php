<?php

namespace app\models;

use Yii;

/**
 * Description of CategoriasChipax
 *
 * @author leand
 */
class CategoriasChipax {

    public $id;
    public $nombre;
    public $parent_id;
    public $tipo_cuenta_id;
    public $modelName;
    public $parentModelName;
    public $compId;
    public $parentCompId;
    public $depth;
    public $hasChildren;

    public static function getCategoriaById($id) {
        $listCategorias = Yii::$app->session->get("Categorias");
        $categoria = null;
        foreach ($listCategorias as $cat) {
            if ($cat["id"] == $id) {
                $categoria = new CategoriasChipax();
                $categoria->id = $cat["id"];
                $categoria->nombre = $cat["nombre"];
                $categoria->parent_id = $cat["parent_id"];
                $categoria->tipo_cuenta_id = $cat["tipo_cuenta_id"];
                $categoria->modelName = $cat["modelName"];
                $categoria->parentModelName = $cat["parentModelName"];
                $categoria->compId = $cat["compId"];
                $categoria->parentCompId = $cat["parentCompId"];
                $categoria->depth = $cat["depth"];
                $categoria->hasChildren = $cat["hasChildren"];
                return $categoria;
            }
        }
        return $categoria;
    }

}
