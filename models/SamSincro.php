<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sam_sincro".
 *
 * @property int $id
 * @property string $fecha
 * @property string $folio
 * @property int $monto
 * @property string $proveedor
 * 
 */
class SamSincro extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'sam_sincro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['fecha', 'folio'], 'required'],
            [['fecha', 'monto'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'folio' => 'Folio',
            'monto' => 'Neto',
            'proveedor' => 'Proveedor'
        ];
    }

    public static function isSamSincronizedFolio($folio, $proveedor) {
        if ($folio == "") 
            return false;
        $samDato = SamSincro::find()->where(['like', "folio", $folio])
                ->andWhere(["like", "proveedor", $proveedor])->one();
        
        if (isset($samDato)) {
            return true;
        } else {
            return false;
        }
    }
    
}
