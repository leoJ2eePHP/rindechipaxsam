<?php

namespace app\controllers;

use Yii;
use app\models\RindeGastos;

class RindeGastosController extends \yii\web\Controller {

    public function actionIndex() {
        $rindeGastos = new RindeGastos(Yii::$app->params["rindeGastosToken"]);
        $params['ResultsPerPage'] = 1000;
        $params['Since'] = '2021-06-01';
        $params['Until'] = '2021-06-30';
        $params['ExpensePolicyId'] = Yii::$app->params["rgIdDeptoMaquinarias"];
        $json = $rindeGastos->getExpenses($params);
        
        $gastos = json_decode($json);
        return $this->render("index", ["model" => $gastos->Expenses]);
    }
    
}
