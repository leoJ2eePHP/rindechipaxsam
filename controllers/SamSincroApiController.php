<?php
namespace app\controllers;

/**
 * Description of UserController
 *
 * @author leand
 */
class SamSincroApiController extends \yii\rest\ActiveController {
    
    public $modelClass = "app\models\SamSincro";
    
    protected function verbs() {
        return [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'update-sincro' => ['POST']
            ],
        ];
    }
    
    public function actions() {
        $actions = parent::actions();
        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update'], $actions["view"]);
        // customize the data provider preparation with the "prepareDataProvider()" method
        //$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }
    
    public function actionUpdateSincro() {
        $post = \Yii::$app->request->post();
        
        $samSincro = \app\models\SamSincro::find()->where("folio = :folio AND proveedor = :prove",
                [":folio" => $post["folio"], ":prove" => $post["proveedor"]])->one();
        
        if ($samSincro != null) {
            $samSincro->delete();
            return "OK";
        } else {
            return "ERROR";
        }
        
        return $samSincro;
    }
    
}