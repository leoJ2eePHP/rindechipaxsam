<?php

namespace app\controllers;

use yii\httpclient\Client;
use app\models\FlujoCajaCartola;

class ChipaxApiController extends \yii\rest\ActiveController {

    public $modelClass = 'app\models\Vacio';
    public $token = "";

    public function actions() {
        $actions = parent::actions();
        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update'], $actions['index']);
        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    protected function verbs() {
        return [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['GET'],
                'generate-token' => ['GET'],
                'otra-llamada' => ['GET'],
                'testing' => ['GET'],
            ],
        ];
    }

    private function generateToken() {
        $client = new Client();
        $this->token = $client->createRequest()
                ->setMethod("POST")
                ->setUrl("https://api.chipax.com/login")
                ->setData(["app_id" => \Yii::$app->params["app_id"], "secret_key" => \Yii::$app->params["secret_key"]])
                ->send();

        return $this->token->getData();
    }

    public function actionListaClientes() {
        $client = new Client(["baseUrl" => "https://api.chipax.com/clientes"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();
        
        return $request->getData();
    }

    public function actionDtes() {
        $client = new Client(["baseUrl" => "https://api.chipax.com/dtes"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();
        
        return $request->getData();
    }
    
    public function actionLineasNegocio() {
        $client = new Client(["baseUrl" => "https://api.chipax.com/lineas-negocio"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();
        
        return $request->getData();
    }
    
    public function actionFlujoCajaCartolas() {
        $client = new Client(["baseUrl" => "https://api.chipax.com/flujo-caja/cartolas"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();
        
        return FlujoCajaCartola::convert2Model($request->getData()["docs"]);
        
        //return $flujo;
        //return $request->getData()["docs"];
    } 
    
    public function actionFlujoCaja() {
        $client = new Client(["baseUrl" => "https://api.chipax.com/flujo-caja/init"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();
        
        return $request->getData();
    }
    
    public function actionFlujoCajaCategorias() {
        $client = new Client(["baseUrl" => "https://api.chipax.com/flujo-caja/categorias"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();
        
        return $request->getData();
    }
   
    public function actionCartolas() {
        $client = new Client(["baseUrl" => "https://api.chipax.com/flujo-caja/cartolas"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();
        
        return $request->getData();
    }
    
    public function actionMovimientos() {
        $client = new Client(["baseUrl" => "https://api.chipax.com/movimientos"]);
        $request = $client->createRequest()
                ->setHeaders(['content-type' => 'application/json'])
                ->addHeaders(['Authorization' => 'JWT ' . $this->getToken()["token"]])
                ->send();
        
        return $request->getData();
    }
    
    private function getToken() {
        if (!isset($this->token) || empty($this->token)) {
            $this->token = $this->generateToken();
        }
        /*
          $cache = new \yii\caching\FileCache();
          $data = $cache->get("token");
          if ($data === false) {
          $cache->set("token", $this->generateToken());
          return $cache->get("token");
          }
         */
        return $this->token;
    }

}
