<?php

namespace app\controllers;

use Yii;
use app\models\RindeGastos;

class RindeGastosApiController extends \yii\rest\ActiveController {

    public $modelClass = 'app\models\Vacio';
    public $token = "";

    public function actions() {
        $actions = parent::actions();
        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update'], $actions['index']);
        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    protected function verbs() {
        return [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['GET'],
                'generate-token' => ['GET'],
                'otra-llamada' => ['GET'],
                'testing' => ['GET'],
            ],
        ];
    }

    public function actionExpenses() {
        $rindeGastos = new RindeGastos(Yii::$app->params["rindeGastosToken"]);
        $params['ResultsPerPage'] = 1000;
        //$params['Page'] = 1;
        $params['Since'] = '2021-06-01';
        $params['Until'] = '2021-06-30';
//        $params['Currency'] = 'CLP';
//        $params['Status'] = 0;
//        $params['Category'] = 'Travel';
//        $params['ReportId'] = 1;
        $params['ExpensePolicyId'] = Yii::$app->params["rgIdDeptoMaquinarias"];
//        $params['UserId'] = 2;
//        $params['OrderBy'] = 1;
//        $params['Order'] = 'ASC';
        $json = $rindeGastos->getExpenses($params);
        $gastos = json_decode($json);
        
        return $gastos->Expenses;
        //return $gastos->Expenses;
    }

    public function actionPoliticasGastos() {
        $rindeGastos = new RindeGastos(Yii::$app->params["rindeGastosToken"]);
        $params['Status'] = 1;
        $params['Page'] = 1;
        $JSON_expenses = $rindeGastos->getExpensePolicies($params);
    }
    
}
