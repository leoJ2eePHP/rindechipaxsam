<?php

namespace app\controllers;

use app\models\ChipaxApiModel;
use app\models\RindeGastos;
use app\components\Helper;

use Yii;

class ChipaxController extends \yii\web\Controller {

    public function actionIndex($hash = null) {       
        $session = Yii::$app->session;
        if ($hash == null) {
            if ($session->has('hash')) {
                $hash = $session->get('hash');
            }
        } else {
            if ($session->has('hash')) {
                $session->remove('hash');
            }
        }
        $local_hash = Helper::chipaxSecret(0);
        $intentos = 1;
        while ($local_hash != $hash && $intentos <= 30) {
            $local_hash = Helper::chipaxSecret($intentos);
            $intentos++;
        }
        if ($hash != $local_hash) {
            die("Hash incorrecto");
        }
        $chipax = new ChipaxApiModel();
        // Lleno la información que irá en los comboboxes del modal para sincronizar con SAM
        $session = Yii::$app->session;
        $session->set("RindeGastosExpensesPolicyFields", $this->fillRindeGastosExpensePolicyFields());
        $session->set("Categorias", $chipax->getCategorias());
//        $session->set("CategoriasRindeGastos", \app\models\CategoriaRinde::getCategoriasByPolicyIdFromApi(41786));
        $session->set("LineasNegocio", $chipax->getLineasDeNegocio());
        
        $session->set('hash', $hash);
        
        $fecha_desde = date("Y-m-01");
        $fecha_hasta = date("Y-m-d");
        if (\Yii::$app->request->isPost) {
            $fecha_desde = Helper::formatToDBDate(null !== (\Yii::$app->request->post("fecha_desde")) ? \Yii::$app->request->post("fecha_desde") : "");
            $fecha_hasta = Helper::formatToDBDate(null !== (\Yii::$app->request->post("fecha_hasta")) ? \Yii::$app->request->post("fecha_hasta") : "");
            return $this->render("index", [
                        "fecha_desde" => $fecha_desde,
                        "fecha_hasta" => $fecha_hasta,
                        "model" => $chipax->getFlujoCajaCartolas($fecha_desde, $fecha_hasta),
                        "rindeGastos" => $this->getExpensesRindeGastos($fecha_desde, $fecha_hasta),
                        "combustibles" => $this->getCombustiblesRindeGastos($fecha_desde, $fecha_hasta)
            ]);
        } else {
            return $this->render("index", [
                        "model" => $chipax->getFlujoCajaCartolas(),
                        "rindeGastos" => $this->getExpensesRindeGastos(),
                        "fecha_desde" => $fecha_desde,
                        "fecha_hasta" => $fecha_hasta,
                        "combustibles" => $this->getCombustiblesRindeGastos()
            ]);
        }
    }

    private function getExpensesRindeGastos($start_date = null, $end_date = null) {
        $fecha_desde = null !== $start_date ? $start_date : date("Y-m-d", strtotime(date("Y-m-d") . " - 1 month"));
        $fecha_hasta = null !== $end_date ? $end_date : date("Y-m-d", strtotime(date("Y-m-d") . " + 1 days"));

        $rindeGastos = new RindeGastos(Yii::$app->params["rindeGastosToken"]);
        $params['Status'] = 1;      // Filtrar por solo aprobados
        $params['ResultsPerPage'] = 1000;
//        $params['Since'] = '2021-06-01';
//        $params['Until'] = '2021-07-05';
        $params['Since'] = $fecha_desde;
        $params['Until'] = $fecha_hasta;
        $params['ExpensePolicyId'] = Yii::$app->params["rgIdDeptoMaquinarias"];
        $json = $rindeGastos->getExpenses($params);

        $gastos = json_decode($json);
/*
        $sync = new \app\models\Sincronizacion();
        $sync->fecha = date('Y-m-d H:m:s');
        $sync->save();
        foreach ($gastos->Expenses as $g) {
            $datos = new \app\models\Datos();
            $datos->folio = "" . $g->ExtraFields[2]->Value;
            $datos->monto = $g->Net;
            //$datos->tipo_gasto = "Compras";
            //$datos->detalles = "";
            $datos->sincronizacion_id = $sync->id;
//            if (!$datos->save()) {
//                \Yii::$app->session->setFlash("danger", $datos->getFirstErrors());
//            }
            // SOLO TESTING:
//            if ($g->ExtraFields[2]->Value == 90701 && $g->Net == 4200) {
//                $g->Net = 14500;
//            }
        }
*/
        return $gastos->Expenses;
    }

    public function getCombustiblesRindeGastos($start_date = null, $end_date = null) {
        $fecha_desde = null !== $start_date ? $start_date : date("Y-m-d", strtotime(date("Y-m-d") . " - 1 month"));
        $fecha_hasta = null !== $end_date ? $end_date : date("Y-m-d", strtotime(date("Y-m-d") . " + 1 days"));

        $rindeGastos = new RindeGastos(Yii::$app->params["rindeGastosToken"]);
        $params['Status'] = 1;      // Filtrar por solo aprobados
        $params['ResultsPerPage'] = 1000;
//        $params['Since'] = '2021-06-01';
//        $params['Until'] = '2021-07-05';
        $params['Since'] = $fecha_desde;
        $params['Until'] = $fecha_hasta;
        $params['ExpensePolicyId'] = Yii::$app->params["rgIdCombustibles"];
        $json = $rindeGastos->getExpenses($params);

        $gastos = json_decode($json);
/*
        $sync = new \app\models\Sincronizacion();
        $sync->fecha = date('Y-m-d H:m:s');
        $sync->save();
        foreach ($gastos->Expenses as $g) {
//            $datos = new \app\models\Datos();
//            $datos->folio = "" . $g->ExtraFields[2]->Value;
//            $datos->monto = $g->Net;
            //$datos->tipo_gasto = "Compras";
            //$datos->detalles = "";
//            $datos->sincronizacion_id = $sync->id;
//            if (!$datos->save()) {
//                \Yii::$app->session->setFlash("danger", $datos->getFirstErrors());
//            }
        }
*/
        return $gastos->Expenses;
    }

    public function actionCreateNotaVenta() {
        return $this->renderPartial("_notaVenta");
    }
    
    private function fillRindeGastosExpensePolicyFields() {
        // PRIMERO obtengo de Chipax la línea de negocio a la cual pertenece el gasto
        //$linea_negocio_id = $prorrata["linea_negocio_id"];
        //$negocio = $chipax->getLineasDeNegocioById($linea_negocio_id);

        // LUEGO, busco todas las Políticas de Gastos en RindeGastos, para obtener un ID a partir de una coincidencia
        $rindeGastos = new RindeGastos(Yii::$app->params["rindeGastosToken"]);
        $params['ResultsPerPage'] = 1000;
        //$resultado = $rindeGastos->getExpensePolicies($params);
        //$politicas = json_decode($resultado);

        // BUSCO una coincidencia entre la línea de negocio de Chipax y la política de RindeGastos
        //$politicaNegocio = \app\components\Helper::mostSimilarTextInArray($negocio->nombre, $politicas->Policies);

        // Con el objeto ya comparado, busco sus ExtraFields por su ID
        // $params["IdPolicy"] = $politicaNegocio->Id;
        $params["IdPolicy"] = 41786;    // Se cambia a que busque siempre por la política de Maquinas, para que traiga vehículos y unidades
        $json = $rindeGastos->getExpensePolicyExpenseFields($params);
        return RindeGastos::convert2PoliticaGastosForm($json);
    }
    
    public function actionSincronizarConChipax() {
        $json = file_get_contents("php://input");
        $data = json_decode($json);

        $carga = new \app\models\CargaMasivaForm();
        $carga->generarExcel($data);
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return "ok";
    }
    
    public function actionDownloadExcel() {
        $full_path = \Yii::getAlias("@app") . DIRECTORY_SEPARATOR . \app\models\CargaMasivaForm::COMPLETE_FILE_PATH;
        \app\components\Helper::download_file($full_path);
    }

}
