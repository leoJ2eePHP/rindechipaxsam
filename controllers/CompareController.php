<?php

namespace app\controllers;

use yii\httpclient\Client;
use app\models\FlujoCajaCartola;
use app\models\RindeGastos;
use Yii;

class CompareController extends \yii\web\Controller {

    public function actionIndex() {
        $chipax = new \app\models\ChipaxApiModel();
        $cartolasChipax = $chipax->getFlujoCajaCartolas();
        $rindeGastos = new RindeGastos(Yii::$app->params["rindeGastosToken"]);
        $params['ResultsPerPage'] = 1000;
        $params['Since'] = '2021-06-01';
        //$params['Until'] = '2021-06-30';
        $params['ExpensePolicyId'] = Yii::$app->params["rgIdDeptoMaquinarias"];
        $json = $rindeGastos->getExpenses($params);

        $gastos = json_decode($json);

        $folios = array();
        foreach ($cartolasChipax as $c) {
            foreach ($c->compras as $com) {
                $folios[] = isset($com->folio) ? $com->folio : 0;
                //$folios[$com->folio] = (isset($folios[$com->folio]) != 0 ? $folios[$com->folio] : 0) + $com->monto_total;
            }
            foreach ($c->gastos as $gasto) {
                $folios[] = isset($gasto->num_documento) ? $gasto->num_documento : 0;
            }
        }

        $ids = array();
        foreach ($gastos->Expenses as $g) {
            $ids[] = isset($g->ExtraFields[2]->Value) ? $g->ExtraFields[2]->Value : 0;
            //$ids[trim($g->ExtraFields[2]->Value)] = (isset($ids[$g->ExtraFields[2]->Value]) ? $ids[$g->ExtraFields[2]->Value] : 0) + $g->Total;
        }

        echo "Cantida de gastos en Chipax: " . count($folios);
        echo "Cantidad de gastos en RindeGastos: " . count($ids);
        
        $diferencia = array();
        foreach ($folios as $fo) {
            foreach ($ids as $i) {
                if ($fo != $i) {
                    $diferencia[] = $fo;
                }
            }
        }
        print_r($diferencia);
        /*
        echo "CHIPAX";
        print_r($folios);
        
        echo "<br>";
        
        echo "RINDEGASTOS";
        print_r($ids);
        */
        die;
    }

}
