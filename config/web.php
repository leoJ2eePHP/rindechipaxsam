<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'America/Santiago',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'z_ucqFxcWdnNTdP6xecpt_dlTB7wrpdW',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['site/index'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/views'
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller' => ["chipax", "rinde-gastos", 'chipax-api', 'rinde-gastos-api', 'compare', 'modal', "sam-sincro-api"],
                    'extraPatterns' => [
                        'GET lista-clientes' => 'lista-clientes',
                        'GET lineas-negocio' => 'lineas-negocio',
                        'GET dtes' => 'dtes',
                        'GET flujo-caja' => 'flujo-caja',
                        'GET flujo-caja-cartolas' => 'flujo-caja-cartolas',
                        'GET flujo-caja-categorias' => 'flujo-caja-categorias',
                        'GET cartolas' => 'cartolas',
                        'GET movimientos' => 'movimientos',
                        'GET expenses' => 'expenses',
                        'GET politicas-gastos' => 'politicas-gastos',
                        'GET compare' => 'compare',
                        'GET index' => 'index',
                        'POST index' => 'index',
                        'GET sync-sam' => 'sync-sam',
                        'POST sync-sam-post' => 'sync-sam-post',
                        'GET upload-dte' => 'upload-dte',
                        'POST upload-dte' => 'upload-dte',
                        'POST update-sincro' => 'update-sincro',
                        'POST sincronizar-con-chipax' => 'sincronizar-con-chipax',
                        'GET download-excel' => 'download-excel'
                    ]
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
