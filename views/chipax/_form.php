<?php

use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Curso */
/* @var $form yii\widgets\ActiveForm */
?>
<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
<?=

$form->field($model, 'profesor_id')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(app\models\Profesor::find()->orderBy("nombre ASC")->all(), "id", "completeName"),
    'options' => ['placeholder' => 'Profesor Jefe', "id" => "profesorJefe"],
    'theme' => 'default',
    //'size' => 'sm',
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);
?>