<?php

use yii\helpers\Html;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CursoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gastos en sistema RindeGastos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="curso-index">

    <div class="card card-info">
        <div class="card-header with-border">
            <h3 class="card-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?= app\components\Alert::widget() ?>
            <p>
                <?php // Html::a('Crear Curso', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <table class="table table-bordered table-striped">
                <thead>
                    <tr class="bg-info">
                        <td style="text-overflow: ellipsis; width: 250px;">Razón Social</td>
                        <td style="width: 110px;">Rut Emisor</td>
                        <td>Fecha Emisión</td>
                        <td>Folio</td>
                        <!--<td>Movimientos</td>-->
                        <td>Neto</td>
                        <td>Total</td>
                        <td>Tipo Movimiento</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($model as $gasto):
                        ?>
                    <tr>
                        <td><?= $gasto->Supplier ?></td>
                        <td><?= $gasto->ExtraFields[3]->Value ?></td>
                        <td><?= Helper::formatToLocalDate($gasto->IssueDate) ?></td>
                        <td><?= $gasto->ExtraFields[2]->Value ?></td>
                        <td><?= number_format($gasto->Net, 0, ",", ".") ?></td>
                        <td><?= number_format($gasto->Total, 0, ",", ".") ?></td>
                        <td><?= $gasto->Category ?></td>
                    </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>

</div>